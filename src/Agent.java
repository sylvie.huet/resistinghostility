/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Sylvie.huet
 */
public class Agent {

    int nbAgentThisType;

    double distAutruiInit;

    int id = -1;

    CulturalWorldView[] myCulture; //initialisées dans ordre lecture fichier : muslims, christians, areligious

    CulturalWorldView[] myCultureInit;

    int nbWorldView;

    float alpha;

    float[] opinionGlobalOnOthers;
    double[] opinionOnOtherAgents;
    double[] previousOpinionOnOtherAgents;

    double opOfTheExtremistOnIt;

    ResistingHostility pop;

    String type; // agressor or victim for message agent 
    String group; // areligious, muslims or christians
    String openness; // inclusive or exclusive

    float partInGroup; // part of this agent in the group (taux d'inclusif ou d'exclusif)

    // Constructeur inutile pour permettre une initialisation provisoire
    public Agent() {
    }

    public Agent(ResistingHostility me, int type, int id, String[] name, float[] adherence, float[] alow, float[] amax, int nbAgents, float alpha, int dimAdhesion, int nb) {
        this.nbAgentThisType = nb;
        this.id = id;
        this.pop = me;
        this.opinionGlobalOnOthers = new float[nbAgents];
        this.distAutruiInit = 0.0;
        opinionOnOtherAgents = new double[6];
        previousOpinionOnOtherAgents = new double[6];
        Arrays.fill(opinionOnOtherAgents, 0.0);
        Arrays.fill(previousOpinionOnOtherAgents, 0.0);
        if (type == 1) {
            this.type = "inclusive";
        } else {
            this.type = "exclusive";
        }
        this.group = groupFromDimAdh(dimAdhesion);
        Arrays.fill(opinionGlobalOnOthers, 0.0f);
        this.nbWorldView = name.length;
        this.alpha = alpha;
        myCulture = new CulturalWorldView[nbWorldView];
        myCultureInit = new CulturalWorldView[nbWorldView];
        for (int i = 0; i < nbWorldView; i++) { // initialisées dans ordre lecture fichier : muslims, christians, areligious
            myCulture[i] = new CulturalWorldView(name[i], adherence[i], alow[i], amax[i]);
            myCultureInit[i] = new CulturalWorldView(name[i], adherence[i], alow[i], amax[i]);
        }
    }

    // This is the agent constructor for the source
    public Agent(ResistingHostility me, String type, float[] adherence, float[] alow, float[] amax, float alpha, int dimAdhesion, int nb) {
        this.nbAgentThisType = nb;
        this.distAutruiInit = 0.0;
        this.type = type;
        this.pop = me;
        this.group = groupFromDimAdh(dimAdhesion);
        this.nbWorldView = adherence.length;
        this.alpha = alpha;
        myCulture = new CulturalWorldView[nbWorldView];
        myCultureInit = new CulturalWorldView[nbWorldView];
        for (int i = 0; i < nbWorldView; i++) {
            myCulture[i] = new CulturalWorldView(type, adherence[i], alow[i], amax[i]);
            myCultureInit[i] = new CulturalWorldView(type, adherence[i], alow[i], amax[i]);
        }
    }

    // This is the agent for generating relevant CWW
    public Agent(String openness, float[] adherence, float[] alow, float[] amax, float alpha, int nb) {
        this.nbAgentThisType = nb;
        this.distAutruiInit = 0.0;
        this.type = type;
        this.openness = openness;
        //this.pop = me;
        this.nbWorldView = adherence.length;
        this.alpha = alpha;
        myCulture = new CulturalWorldView[nbWorldView];
        for (int i = 0; i < nbWorldView; i++) {
            myCulture[i] = new CulturalWorldView(type, adherence[i], alow[i], amax[i]);
        }
    }

    public Agent(String group, String openness, float representativity, float[] adherence, float[] alow, float[] amax, float alpha, int nb) {
        this.nbAgentThisType = nb;
        this.distAutruiInit = 0.0;
        this.group = group;
        this.openness = openness;
        this.partInGroup = representativity;
        this.nbWorldView = adherence.length;
        this.alpha = alpha;
        myCulture = new CulturalWorldView[nbWorldView];
        for (int i = 0; i < nbWorldView; i++) {
            myCulture[i] = new CulturalWorldView(type, adherence[i], alow[i], amax[i]);
        }
    }

    // This is the agent for generating relevant CWW
    public Agent(Agent A) {
        this.nbAgentThisType = A.nbAgentThisType;
        this.distAutruiInit = A.distAutruiInit;
        this.group = A.group;
        this.openness = A.openness;
        this.partInGroup = A.partInGroup;
        //this.pop = me;
        this.nbWorldView = A.nbWorldView;
        this.alpha = A.alpha;
        myCulture = new CulturalWorldView[nbWorldView];
        float[] adherence = new float[nbWorldView];
        float[] alow = new float[nbWorldView];
        float[] amax = new float[nbWorldView];
        for (int i = 0; i < nbWorldView; i++) {
            adherence[i] = A.myCulture[i].maPosition;
            alow[i] = A.myCulture[i].alow;
            amax[i] = A.myCulture[i].amax;
        }
        for (int i = 0; i < nbWorldView; i++) {
            myCulture[i] = new CulturalWorldView(type, adherence[i], alow[i], amax[i]);
        }
    }

    public double computeOpinionOnOtherMAPosition(float ad, CulturalWorldView c) {
        double w = 0.0f; // opinion about another agent's maPosition at the cultural worldview k
        double temp = 0;
        if (ad == c.maPosition) {
            ad = ad + 0.000001f;
        }
        try {
            if ((ad < c.amax) && (ad > c.alow)) {
                w = 1.0;
            } else if (ad <= c.alow) {
                temp = Math.exp(1 + ((ad - c.maPosition) / (c.maPosition - c.alow)));
                w = (float) ((temp - 1) / (temp + 1));
            } else if (ad >= c.amax) {
                temp = Math.exp(1 + ((c.maPosition - ad) / (c.amax - c.maPosition)));
                w = (float) ((temp - 1) / (temp + 1));
            }
            if (Double.isNaN(w)) {
                throw new Exception(" w is not a number " + c.alow + " " + c.maPosition + " " + c.amax + " ad " + ad + " w = " + w);
            }

        } catch (Exception error) {
            error.printStackTrace();
        }
        return w;
    }

    public double computeOpinionOnOtherMAPosition(float ad, CulturalWorldView c, boolean show) {
        double w = 0.0f; // opinion about another agent's maPosition at the cultural worldview k
        double temp = 0;
        if (ad == c.maPosition) {
            ad = ad + 0.000001f;
        }
        try {
            if ((ad < c.amax) && (ad > c.alow)) {
                w = 1.0;
            } else if (ad <= c.alow) {
                temp = Math.exp(1 + ((ad - c.maPosition) / (c.maPosition - c.alow)));
                w = (float) ((temp - 1) / (temp + 1));
            } else if (ad >= c.amax) {
                temp = Math.exp(1 + ((c.maPosition - ad) / (c.amax - c.maPosition)));
                w = (float) ((temp - 1) / (temp + 1));
            }
            if (Double.isNaN(w)) {
                throw new Exception(" w is not a number " + c.alow + " " + c.maPosition + " " + c.amax + " ad " + ad + " w = " + w);
            }
            if (show) {
                System.err.print(" dot " + ad + " jugeur " + c.alow + " " + c.maPosition + " " + c.amax + " w = " + w + " ");
            }
        } catch (Exception error) {
            error.printStackTrace();
        }
        return w;
    }

    public double opinionOnOthers(Agent A) {
        double opTot = 0.0f;
        double opIJ = 0.0f;
        for (int i = 0; i < nbWorldView; i++) {
            opIJ = opinionAboutOtherAgentForOneCWW(myCulture[i], A.myCulture[i], pop.nbPoints);
            opTot = opTot + opIJ;
        }
        return opTot / (float) nbWorldView;
    }

    public double opinionOnOthers(Agent A, int nbPoints) {
        double opTot = 0.0f;
        double opIJ = 0.0f;
        for (int i = 0; i < nbWorldView; i++) {
            opIJ = opinionAboutOtherAgentForOneCWW(myCulture[i], A.myCulture[i], nbPoints);
            opTot = opTot + opIJ;
        }
        return opTot / (float) nbWorldView;
    }

    public double opinionOnOthers(Agent A, boolean notfromInitState) {
        // I have changed but I don't know the other has changed
        double opTot = 0.0f;
        double opIJ = 0.0f;
        for (int i = 0; i < nbWorldView; i++) {
            if (notfromInitState) {
                opIJ = opinionAboutOtherAgentForOneCWW(myCulture[i], A.myCulture[i], pop.nbPoints);
            } else if ((A.group.equalsIgnoreCase(this.group)) && (A.type.equalsIgnoreCase(this.type))) {
                opIJ = opinionAboutOtherAgentForOneCWW(myCulture[i], myCulture[i], pop.nbPoints);
            } else {
                opIJ = opinionAboutOtherAgentForOneCWW(myCulture[i], A.myCultureInit[i], pop.nbPoints);
            }
            opTot = opTot + opIJ;
        }
        return opTot / (double) nbWorldView;
    }

    public double opinionAboutOtherAgentForOneCWW(CulturalWorldView cI, CulturalWorldView cJ, int nbPoints) { // an agent's J cultural worldview
        double opI = 0.0f;
        double temp = 0.0f;
        float[] op = sampleTolSegmentFromGrid(nbPoints, cJ);
        for (int i = 0; i < op.length; i++) {
            opI = computeOpinionOnOtherMAPosition(op[i], cI);
            temp = temp + opI;
        }
        temp = temp / (double) op.length;
        return temp;
    }

    public double opinionAboutOtherAgentForOneCWW(CulturalWorldView cI, CulturalWorldView cJ, int nbPoints, boolean show) { // an agent's J cultural worldview
        double opI = 0.0f;
        double temp = 0.0f;
        float[] op = sampleTolSegmentFromGrid(nbPoints, cJ);
        for (int i = 0; i < op.length; i++) {
            opI = Agent.this.computeOpinionOnOtherMAPosition(op[i], cI, show);
            temp = temp + opI;
        }
        temp = temp / (double) op.length;
        return temp;
    }

    public float[] sampleTolSegmentBugged(int nbPoints, CulturalWorldView cJ) {
        float[] s = new float[nbPoints];
        // we sample starting from the maPosition of the sampling segment and picked out the same number of point on the left and on the right
        int countP = 0;
        if (nbPoints % 2 == 1) { // nb Points est impair
            // a first sampled point at the maPosition value
            s[countP] = 1.0f; // by construction
            countP++;
            nbPoints = nbPoints - 1;
        }
        nbPoints = nbPoints / 2;
        // sampling on the right side
        float dist = cJ.amax - cJ.maPosition;
        float interv = dist / (float) (nbPoints);
        float depart = cJ.maPosition + (0.5f * interv);
        for (int i = 0; i < nbPoints; i++) {
            s[countP] = depart + (float) i * interv;
            countP++;
        }
        // sampling on the left side
        dist = cJ.maPosition - cJ.alow;
        interv = dist / (float) (nbPoints);
        depart = cJ.alow + (0.5f * interv);
        for (int i = 0; i < nbPoints; i++) {
            s[countP] = depart + (float) i * interv;
            countP++;
        }
        return s;
    }

    public float[] sampleTolSegment(int nbPoints, CulturalWorldView cJ) {
        float dist = cJ.amax - cJ.alow;
        float interv = dist / (float) (nbPoints);
        float depart = cJ.alow + (0.5f * interv);
        float[] s = new float[nbPoints];
        for (int i = 0; i < nbPoints; i++) {
            s[i] = depart + (float) i * interv;
        }
        return s;
    }

    public float[] sampleTolSegmentFromGrid(int nbPoints, CulturalWorldView cJ) { // allow to approximate the segment using always the same dots for everyone
        float interv = 2.0f / (float) (nbPoints); // this is constant 
        float depart = -1.0f;
        float gridDot = -1.0f;
        float reserv = -1.0f;
        for (int i = 0; i <= nbPoints; i++) {
            gridDot = depart + (float) i * interv;
            if (gridDot > cJ.alow) { // we take the closer in the segment
                depart = gridDot;
                reserv = gridDot - interv;
                i = nbPoints + 1;
            }
        }
        ArrayList<Float> points = new ArrayList<Float>();
        float gridPoint = 0.0f;
        for (int i = 0; i <= nbPoints; i++) {
            gridPoint = depart + (float) i * interv;
            if (gridPoint <= cJ.amax) {
                points.add(gridPoint);
            } else {
                i = nbPoints + 1;
            }
        }
        float[] s;
        if (points.size() > 0) {
            s = new float[points.size()];
            for (int i = 0; i < points.size(); i++) {
                s[i] = points.get(i).floatValue();
            }
        } else {
            s = new float[1];
            s[0] = reserv;
        }
        return s;
    }

    public void reactionToMessage(int type, Agent emetteur) {
        reactionToThreatMessageOneDim(emetteur);
    }

    public void reactionToThreatMessageOneDim(Agent emetteur) {
        // I (this) compute what the "emetteur" think of me 
        opOfTheExtremistOnIt = emetteur.opinionOnOthers(this);
        int dimToChange = emetteur.mainDimension();
        if (opOfTheExtremistOnIt < 0.0f) {
            float mu = alpha * (float) ((Math.exp(opOfTheExtremistOnIt) - 1) / (Math.exp(opOfTheExtremistOnIt) + 1));
            myCulture[dimToChange].changeBoundIfThreat(mu, emetteur.myCulture[dimToChange].maPosition, pop.epsilon);
        }
    }

    public int mainDimension() {
        int dim = 0;
        float adherMax = Float.MIN_VALUE;
        for (int i = 0; i < myCulture.length; i++) {
            if (myCulture[i].maPosition > adherMax) {
                adherMax = myCulture[i].maPosition;
                dim = i;
            }
        }
        return dim;
    }

    public int mainCWWFromGroup() {
        int cww = 0; // muslims
        if (this.group.equals(new String("areligious"))) {
            cww = 2;
        } else if (this.group.equals(new String("christians"))) {
            cww = 1;
        }
        return cww;
    }

    //
    public String groupFromDimAdh(int dimAdhesion) {
        String g = new String("muslims");
        if (dimAdhesion == 2) {
            g = new String("areligious");
        } else if (dimAdhesion == 1) {
            g = new String("christians");
        }
        return g;
    }
}
