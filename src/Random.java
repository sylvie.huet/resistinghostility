/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import umontreal.iro.lecuyer.probdist.ExponentialDistFromMean;
import umontreal.iro.lecuyer.randvar.ExponentialGen;
import umontreal.iro.lecuyer.randvar.NormalGen;
import umontreal.iro.lecuyer.randvar.RandomVariateGen;
import umontreal.iro.lecuyer.rng.MRG32k3a;
import umontreal.iro.lecuyer.rng.RandomPermutation;

/**
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 * @author sylvie.huet
 */
public class Random {

    private MRG32k3a randomStream;

    /**
     * This method returns an integer picked out following a Uniform law between i and i1. The values i and i1 are included (can be picked out)
     */
    public int nextInt(int i, int i1) {
        return randomStream.nextInt(i, i1);
    }

    /**
     * This method returns a double picked out following a Uniform law between 0 and 1 with 0 and 1 excluded (can't be picked out)
     */
    public double nextDouble() {
        return randomStream.nextDouble();
    }

    public int nextIndexWithDistribution(float[] distribution) {
        return nextIndexWithDistribution(distribution, DoubleCollectionIterator.fromFloatPrimitive);
    }

    public int nextIndexWithDistribution(Double[] distribution) {
        return nextIndexWithDistribution(distribution, DoubleCollectionIterator.fromDoubleClass);
    }

    public int nextIndexWithDistribution(List<Double> distribution) {
        return nextIndexWithDistribution(distribution, DoubleCollectionIterator.fromDoubleClassList);
    }

    /**
     * Picks out randomly an object in the given list.
     * @param <T>
     * @param objects
     * @return 
     */
    public <T> T nextObject(List<T> objects) {
        return nextObject(objects, false);
    }

    /**
     * Picks out randomly an object in the given list.
     * @param <T>
     * @param objects
     * @param remove if true, the returned object is removed from the list
     * @return null if the list is null or empty.
     */
    public <T> T nextObject(List<T> objects, boolean remove) {
        if (objects == null || objects.isEmpty()) {
            return null;
        }
        if (remove) {
            return objects.remove(nextInt(0, objects.size() - 1));
        } else {
            return objects.get(nextInt(0, objects.size() - 1));
        }
    }

    /**
     * Pick out an object in the values of a map, according to the probilities stored in the keys
     */
    public <T> T nextMapObjectWithDistributionInKeys(SortedMap<Double, T> map) {
        double rando = nextDouble();
        double value = map.firstKey();
        Iterator<Double> keysIterator = map.keySet().iterator();
        double sum = keysIterator.next();
        while (keysIterator.hasNext()) {
            sum += value;
            if (sum >= rando) {
                break;
            }
            value = keysIterator.next();
        }
        return map.get(value);
    }

    public int nextIndexWithDistribution(double[] distribution) {
        return nextIndexWithDistribution(distribution, DoubleCollectionIterator.fromDoublePrimitive);
    }

    /**
     * Pick an index in an array covered by a distribution (non-cumulative). If the sum
     * of probabilities is less than 1, it may occur that the random index is out of the
     * given probabilities. In this case, the last index is returned.
     * 
     * @param distribution
     * @return the selected index or -1 if out of distribution.
     */
    public <T> int nextIndexWithDistribution(T distribution, DoubleCollectionIterator<T> iterator) {
        double rando = nextDouble();
        int index = 0;
        double sum = iterator.get(distribution, index);
        while (rando > sum) {
            index++;
            if (index == iterator.size(distribution)) {
                break;
            }
            sum += iterator.get(distribution, index);
        }
        if (index == iterator.size(distribution)) {
            // return the last index
            index--;
        }

        return index;
    }

    /**
     * Use this method to initialize a generator based on a statistical distribution.
     * For example, this code create a generator from a normal distribution:
     * <p><code>NormalGen normalGenerator = new NormalACRGen(null, 0, 1);<br/>
     * Random.setRandomStreamInDist(normalGenerator);
     * </code></p>
     * @param rvg The distribution-based generator to initialize
     */
    public void setRandomStreamInDist(RandomVariateGen rvg) {
        rvg.setStream(randomStream);
    }

    /**
     * This method return a random order for a sequence of
     * leng numbers.
     */
    public int[] randomList(int leng) {
        int[] vec = new int[leng];
        for (int i = 0; i < leng; i++) {
            vec[i] = i;
        }
        RandomPermutation.shuffle(vec, randomStream);
        return vec;
    }

    public String getSeedToString() {
        StringBuilder buff = new StringBuilder();
        for (long val : randomStream.getState()) {
            buff.append(val);
            buff.append("   ");
        }
        return buff.toString();
        // this code make troubles in exception stacktrace formatting (don't know whyâ€¦)
        //return Arrays.toString(randomStream.getState());
    }

    /**
     * This method should only be called by the simulation starter.
     * The initial status for the RNG is initialized by the simulator at startup.
     * @param seed
     */
    public void setSeed(long[] seed) throws ProcessingException {
        if (randomStream != null) {
            throw new ProcessingException("Trying to reinit the seed of the RNG although it is already initialized!");
        }
        randomStream = new MRG32k3a();
        randomStream.setSeed(seed);
    }

    public ExponentialGen createExponentialGenFromMean(float mean) {
        return new ExponentialGen(randomStream, new ExponentialDistFromMean(mean));
    }

    /**
     * Pick out a float following a Normal law
     */
    public float pickOutNormalLaw(float m, float std) {
        return (float) NormalGen.nextDouble(randomStream, m, std);
    }

    public double pickOutNormalLaw(double m, double std) {
        return NormalGen.nextDouble(randomStream, m, std);
    }

    /**
     * Randomly permutes list. This method permutes the whole list.
     * @param list the list to process
     */
    public void shuffle(List list) {
        RandomPermutation.shuffle(list, randomStream);
    }

    /**
     * Picks randomly some elements from a list.
     * @param <E>
     * @param list
     * @param nb the number of elements to pick out
     * @return null, if nb <= 0
     */
    public <E> List<E> pickElements(List<E> list, int nb) {
        if (nb <= 0) {
            return null;
        }
        List<E> result = new ArrayList<E>(nb);
        List<E> copy = new ArrayList<E>(list);
        for (int i = 0; i < nb; i++) {
            int index = nextInt(0, copy.size() - 1);
            result.add(copy.remove(index));
        }
        return result;
    }

    /**
     * Build an array of indexes for randomly picking elements in a matrix.
     * @param elements
     * @return an array of two-dimensional indexes
     */
    public List<int[]> buildRandomMatrixIndexes(Object[][] elements) {
        List<int[]> indexes = new ArrayList<int[]>();
        for (int i = 0; i < elements.length; i++) {
            for (int j = 0; j < elements[i].length; j++) {
                indexes.add(new int[]{i, j});
            }
        }
        shuffle(indexes);
        return indexes;
    }
}
