
import cern.colt.Arrays;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author oumou-raby.dia
 */
public class Tools {

    public boolean go;
    public boolean endFile;
    public PrintStream writResult;
    public String messageFileName;
    public String country; // give the name of the experiment

    public Tools() {
        go = true;
    }

    //Lecture des données d'initialisation des agents
    public float[][] lireExpDesignInitPop(int firstLine, int endLine, String name, int nbDimCulturel) {
        go = true;
        float[][] myPop = new float[0][0];
        Workbook workbook = null;
        try {
            /* Récupération du classeur Excel (en lecture) */
            workbook = Workbook.getWorkbook(new File(name));
            /* Un fichier excel est composé de plusieurs feuilles, on y accède de la manière suivante*/
            Sheet sheet = workbook.getSheet(0);
            int a = 0;
            if (firstLine >= sheet.getRows()) {
                endFile = true;
                go = false;
            } else {
                myPop = new float[nbDimCulturel * 2][0];
                for (int j = firstLine; j <= endLine; j++) {
                    Cell[] lineExpe = sheet.getRow(j);
                    myPop[a] = readLineInitPop(lineExpe, nbDimCulturel);
                    a++;
                }
                go = false;
            }
            workbook.close();
        } catch (BiffException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (workbook != null) {
                /* On ferme le worbook pour libérer la mémoire */
                workbook.close();
            }
        }
        return myPop;
    }

    public float[] readLineInitPop(Cell[] lineExpe, int nbDimCult) {
        float[] toGenere = new float[13];

        String[] expe = new String[lineExpe.length];
        for (int i = 0; i < lineExpe.length; i++) {
            expe[i] = (lineExpe[i].getContents()).replaceAll(",", ".");
        }

        country = expe[0];
        messageFileName = expe[1];
        float taillePop = (new Integer(expe[2])).intValue();
        float dimAdhesion = -1.0f;
        if ((expe[3]).equals("areligious")) {
            dimAdhesion = 2.0f;
        } else if ((expe[3]).equals("christians")) {
            dimAdhesion = 1.0f;
        } else { // is muslim
            dimAdhesion = 0.0f;
        }
        float partTypeInPop = (new Float(expe[4])).floatValue();
        float type = (new Float(expe[5])).floatValue();
        float CWWlow1 = (new Float(expe[6])).floatValue();
        float CWWadherence1 = (new Float(expe[7])).floatValue();
        float CWWup1 = (new Float(expe[8])).floatValue();
        float CWWlow2 = (new Float(expe[9])).floatValue();
        float CWWadherence2 = (new Float(expe[10])).floatValue();
        float CWWup2 = (new Float(expe[11])).floatValue();
        float CWWlow3 = (new Float(expe[12])).floatValue();
        float CWWadherence3 = (new Float(expe[13])).floatValue();
        float CWWup3 = (new Float(expe[14])).floatValue();

        int nbDimAdhes = Math.round(taillePop * partTypeInPop);

        toGenere[0] = dimAdhesion;
        toGenere[1] = nbDimAdhes;
        toGenere[2] = type;
        toGenere[3] = CWWlow1;
        toGenere[4] = CWWadherence1;
        toGenere[5] = CWWup1;
        toGenere[6] = CWWlow2;
        toGenere[7] = CWWadherence2;
        toGenere[8] = CWWup2;
        toGenere[9] = CWWlow3;
        toGenere[10] = CWWadherence3;
        toGenere[11] = CWWup3;
        toGenere[12] = taillePop;

        return toGenere;
    }

    public int[] readLineInitPopOld(Cell[] lineExpe, int nbDimCult) {

        int[] toGenere = new int[8];

        String[] expe = new String[lineExpe.length];
        for (int i = 0; i < lineExpe.length; i++) {
            expe[i] = (lineExpe[i].getContents()).replaceAll(",", ".");
        }

        country = expe[0];
        messageFileName = expe[1];
        int taillePop = (new Integer(expe[2])).intValue();
        int dimAdhesion = (new Integer(expe[3])).intValue();
        float partTypeInPop = (new Float(expe[4])).floatValue();
        float partInclusiveInType = (new Float(expe[5])).floatValue();
        float partExclusiveInType = (new Float(expe[6])).floatValue();
        float partExtremInInclusif = (new Float(expe[7])).floatValue();
        float partExtremInExclusif = (new Float(expe[8])).floatValue();
        int nbDimAdhes = (int) (taillePop * partTypeInPop);
        int inclus = (int) (nbDimAdhes * partInclusiveInType);
        int exclus = nbDimAdhes - inclus;
        int inclusExtr = (int) (inclus * partExtremInInclusif);
        int exclusExtr = (int) (exclus * partExtremInExclusif);

        toGenere[0] = dimAdhesion;
        toGenere[1] = nbDimAdhes;
        toGenere[2] = inclus;
        toGenere[3] = exclus;
        toGenere[4] = inclusExtr;
        toGenere[5] = exclusExtr;
        toGenere[6] = taillePop;
        toGenere[7] = nbDimCult;

        return toGenere;
    }

    public void ecritureInFileSourceStudyFromDataAVGSTD(String nomFile) {
        try {
            writResult = new PrintStream(new FileOutputStream(new File(nomFile)));
            writResult.print("popFile" + ";" + "messageFile" + ";" + "nomExpe" + ";" + "numReplique" + ";" + "randomizedInitCWW" + ";" + "alpha" + ";" + "epsilon" + ";" + "nbPoints" + ";" + "nbDimCWW" + ";");
            writResult.print("iteration" + ";"
                    
                    + "GroupOfAgent(2=Areligious,1=Christians,0=Muslims" + ";" + "typeOfAgent" +";" + "numberOfAgentOfThisType(AreligiousInclusive)"+ ";"
                    + "GroupOfAgent(2=Areligious,1=Christians,0=Muslims" + ";" + "typeOfAgent" +";" + "numberOfAgentOfThisType(AreligiousExclusive)"+ ";"
                    + "GroupOfAgent(2=Areligious,1=Christians,0=Muslims" + ";" + "typeOfAgent" +";" + "numberOfAgentOfThisType(ChristiansInclusive)"+ ";"
                    + "GroupOfAgent(2=Areligious,1=Christians,0=Muslims" + ";" + "typeOfAgent" +";" + "numberOfAgentOfThisType(ChristiansExclusive)"+ ";"
                    + "GroupOfAgent(2=Areligious,1=Christians,0=Muslims" + ";" + "typeOfAgent" +";" + "numberOfAgentOfThisType(MuslimsInclusive)"+ ";"
                    + "GroupOfAgent(2=Areligious,1=Christians,0=Muslims" + ";" + "typeOfAgent" +";" + "numberOfAgentOfThisType(MuslimsExclusive)"+ ";"
                    + "AverageOpinionOfGroup0onGroup0" + ";" + "STDDevOpinionOfGroup0onGroup0" + ";" + "AverageOpinionOfGroup0onGroup1" + ";" + "STDDevOpinionOfGroup0onGroup1" + ";" + "AverageOpinionOfGroup0onGroup2" + ";" + "STDDevOpinionOfGroup0onGroup2" + ";"
                    + "AverageOpinionOfGroup1onGroup0" + ";" + "STDDevOpinionOfGroup1onGroup0" + ";" + "AverageOpinionOfGroup1onGroup1" + ";" + "STDDevOpinionOfGroup1onGroup1" + ";" + "AverageOpinionOfGroup1onGroup2" + ";" + "STDDevOpinionOfGroup1onGroup2" + ";"
                    + "AverageOpinionOfGroup2onGroup0" + ";" + "STDDevOpinionOfGroup2onGroup0" + ";" + "AverageOpinionOfGroup2onGroup1" + ";" + "STDDevOpinionOfGroup2onGroup1" + ";" + "AverageOpinionOfGroup2onGroup2" + ";" + "STDDevOpinionOfGroup2onGroup2" + ";"
                    + "AverageOpinionOfGroup0OnAllTheAgents" + ";" + "STDDevOpinionOfGroup0OnAllTheAgents" + ";" + "AverageOpinionOfGroup1OnAllTheAgents" + ";" + "STDDevOpinionOfGroup1OnAllTheAgents" + ";" + "AverageOpinionOfGroup2OnAllTheAgents" + ";" + "STDDevOpinionOfGroup2OnAllTheAgents" + ";"
                    + "AverageOpinionOfAllOnGroup0members" + ";" + "STDDevOpinionOfAllOnGroup0members" + ";" + "AverageOpinionOfAllOnGroup1members" + ";" + "STDDevOpinionOfAllOnGroup1members" + ";" + "AverageOpinionOfAllOnGroup2members" + ";" + "STDDevOpinionOfAllOnGroup2members" + ";");
             writResult.println();
        } catch (Exception e) {
            System.err.println("Erreur création du fichier pour le résultat de l'algorithme : " + e);
        }
    }

    public void ecritureInFileSourceStudyFromDataDetails(String nomFile) { // for the method : writeImpactMessageWithAgentDetails
        try {
            writResult = new PrintStream(new FileOutputStream(new File(nomFile)));
            writResult.print("popFile" + ";" + "messageFile" + ";" + "nomExpe" + ";" + "numReplique" + ";" + "randomizedInitCWW" + ";" + "alpha" + ";" + "epsilon" + ";" + "nbPoints" + ";" + "nbDimCWW" + ";");
            writResult.print("iteration" + ";" + "numberOfAgentOfThisType" + ";" + "typeOfAgent" + ";" + "GroupOfAgent(2=Areligious,1=Christians,0=Muslims" + ";"
                    + "AgentCWW0Muslim low" + ";" + "AgentCWW0Muslim mostAcceptablePosition" + ";" + "AgentCWW0Muslim up" + ";"
                    + "AgentCWW1Christian low" + ";" + "AgentCWW1Christian mostAcceptablePosition" + ";" + "AgentCWW1Christian up" + ";"
                    + "AgentCWW2Areligious low" + ";" + "AgentCWW2Areligious mostAcceptablePosition" + ";" + "AgentCWW2Areligious up" + ";"
                    + "messageType(1=threat)" + ";" + "sourceMessagetype" + ";" + "groupOfTheSourceMessage" + ";"
                    + "sourceCWW0MuslimMuslim low" + ";" + "sourceCWW0Muslim mostAcceptablePosition" + ";" + "sourceCWW0Muslim up" + ";"
                    + "sourceCWW1Christian low" + ";" + "sourceCWW1Christian mostAcceptablePosition" + ";" + "sourceCWW1Christian up" + ";"
                    + "sourceCWW2Areligious low" + ";" + "sourceCWW2Areligious mostAcceptablePosition" + ";" + "sourceCWW2Areligious up" + ";"
                    + "averageOpinionOfThisTypeOfAgentOnSourceBeforeMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceAfterMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceThreatBeforeMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceThreatAfterMessage" + ";"
                    + "averageOpinionOfThisTypeOfAgentOnSourceMuslimInclusifBeforeMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceMuslimInclusifAfterMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceMuslimExclusifBeforeMessage" + ";"
                    + "averageOpinionOfThisTypeOfAgentOnSourceMuslimExclusifAfterMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceAreligiousInclusifBeforeMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceAreligiousInclusifAfterMessage" + ";"
                    + "averageOpinionOfThisTypeOfAgentOnSourceAreligiousExclusifBeforeMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceAreligiousExclusifAfterMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceChristiansInclusifBeforeMessage" + ";"
                    + "averageOpinionOfThisTypeOfAgentOnSourceChristiansInclusifAfterMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceChristiansExclusifBeforeMessage" + ";" + "averageOpinionOfThisTypeOfAgentOnSourceChristiansExclusifAfterMessage" + ";"
                    + "distAutruiOnlyMeChanged" + ";" + "distAutruiOtherAndMeChanged" + ";" + "distIntraGroupSeeMeOnly" + ";" + "distIntraGroupSeeOthers" + ";" + "knowsAboutOthers" + ";"
                    + "averageOpinionOfThisTypeOfAgentOnEveryone" + ";" + "averageOpinionOfThisTypeOfAgentOnGroup0Muslim" + ";" + "averageOpinionOfThisTypeOfAgentOn1Christian" + ";" + "averageOpinionOfThisTypeOfAgentOnGroup2Areligious" + ";");
            writResult.println();
        } catch (Exception e) {
            System.err.println("Erreur création du fichier pour le résultat de l'algorithme : " + e);
        }
    }

    //label message, indiv source (ou pas), source type, source group, source cwws, opinion receiver on source avant message, opinion receiver on source après message
    //Lecture des données d'initialisation des agents
    public float[][] lireExpDesignInitMessages(String name) {

        float[][] myMessages = new float[0][0];
        Workbook workbook = null;
        try {
            /* Récupération du classeur Excel (en lecture) */
            workbook = Workbook.getWorkbook(new File(name));
            /* Un fichier excel est composé de plusieurs feuilles, on y accède de la manière suivante*/
            Sheet sheet = workbook.getSheet(0);
            int nbMessages = sheet.getRows() - 1;
            myMessages = new float[nbMessages][0];
            for (int j = 0; j < nbMessages; j++) {
                Cell[] lineExpe = sheet.getRow(j + 1);
                myMessages[j] = readLineInitMessage(lineExpe);
            }
        } catch (BiffException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (workbook != null) {
                /* On ferme le worbook pour libérer la mémoire */
                workbook.close();
            }
        }
        return myMessages;
    }

    public float[] readLineInitMessage(Cell[] lineExpe) {

        float[] toGenere = new float[lineExpe.length];

        String[] expe = new String[lineExpe.length];
        for (int i = 0; i < lineExpe.length; i++) {
            expe[i] = (lineExpe[i].getContents()).replaceAll(",", ".");
        }
        toGenere[0] = 2; // 2 is a compassion message
        if (expe[0].equals(new String("threat"))) {
            toGenere[0] = 1; // 1 is a threat
        }
        for (int i = 1; i < lineExpe.length; i++) {
            toGenere[i] = (new Float(expe[i])).floatValue(); // series of triplets : low CWW, adherence CWW, up CWW to characteriste the cultural world view of the source
        }
        return toGenere;
    }

}
