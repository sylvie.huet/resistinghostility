/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author sylvie.huet
 */
public class Message {
    
    int time ; // iteration at which the message is delivered
    
    int type ; // 1 = threat ; 2 = compassion
    
    Agent emetteur ; // id of the agent threatening the other agent or being a victim (possibly benefiting from compassion

    public Message(int time, int type, Agent emetteur) {
        this.time = time ;
        this.type = type;
        this.emetteur = emetteur;
    }
    
    
    
}
