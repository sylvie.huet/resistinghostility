/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Sylvie.huet
 */
public class ResistingHostility {

    /**
     * @param args the command line arguments
     */
    List<Message> messages;
    Agent[] population;
    static int nbWorldView;
    int nbPoints; // nbPoints to sample in j's tolerance segment to define the opinions of i about j 
    int nbIter;
    float epsilon; // min possible tolerance
    float alpha; // Parametre
    int inclusifs;
    int exclusifs;
    int nbMessages;
    float partExclusive;
    static Random myRand;
    int iteration;
    static Tools myPop; // reader of the entering files
    static float[] parametres;

    public int getIteration() {
        return iteration;
    }
    int source; // for the message
    String messageFile;
    String nomExpe;
    static String popFile;
    static boolean expeFini;
    static float randomized; // this is to change slightly and randomly the entering cultural worldviews pattern of agents
    static int sizePop;

    public static void main(String[] args) {
        // parameters by default
        int outputType = 0; // meaning the output is average values of attitudes over the population (1 means details for each agent type)
        popFile = new String("expDesign.xls");
        String nameFileResults = new String("globalResultsAVG.csv");
        sizePop = 1000;
        int nbRepliques = 1; // 1 suffit dans le cas ou pop init pas random car pas d'heuristique de rencontre
        float alpha = 0.5f;
        float epsilon = 0.000001f;
        int nbPoint = 400;
        nbWorldView = 3;
        randomized = 0.0f; // allow to vary slightly the agents (if equal to 0, the agents are only defined by the initialisation from the file)
        // parameters of the user if they are given 
        if (args.length > 0) {
            outputType = new Integer(args[0]).intValue();
            nameFileResults = args[1];
            popFile = args[2];
            sizePop = new Integer(args[3]).intValue();;
            nbRepliques = new Integer(args[4]).intValue();; // 1 suffit dans le cas ou pop init pas random car pas d'heuristique de rencontre
            alpha = new Float(args[5]).floatValue();
            epsilon = new Float(args[6]).floatValue();
            nbPoint = new Integer(args[7]).intValue();;
            nbWorldView = new Integer(args[8]).intValue();;
            randomized = new Float(args[9]).floatValue(); // allow to vary slightly the agents (if equal to 0, the agents are only defined by the initialisation from the file)
        }

        parametres = new float[6];
        parametres[1] = randomized;
        parametres[2] = alpha;
        parametres[3] = epsilon;
        parametres[4] = (float) (nbPoint);
        parametres[5] = (float) (nbWorldView);
        boolean knowTheOthersHaveChanged = true; // to compute the opinions of the population of the group of each agent making them taking into account others have changed or not
        myPop = new Tools();
        if (outputType == 0) { // the user wishes to have the average values of opinion over the population
            myPop.ecritureInFileSourceStudyFromDataAVGSTD(nameFileResults);
        } else { // it is equal to 1 and the user asks for the detailed value for each type of agents
            myPop.ecritureInFileSourceStudyFromDataDetails(nameFileResults);
        }
        try {
            myRand = new Random();
            long stuf = (long) (12345 * Math.random());
            myRand.setSeed(new long[]{stuf, 12345, 12345, 12345, 12345, 12345});
        } catch (Exception e) {
            e.printStackTrace();
        }
        int end = 0;
        int i = 1; //
        float numExpe = 0.0f;
        expeFini = false;
        while (!expeFini) {
            numExpe++;
            for (int j = 0; j < nbRepliques; j++) {
                parametres[0] = (float) (j); // numReplique
                end = i + (nbWorldView * 2) - 1;
                System.err.println("I read and try to launch (if they exist in the file) the experimentations number " + i + " to " + end + " and the replica number " + j);
                new ResistingHostility(alpha, epsilon, nbPoint, i, end, knowTheOthersHaveChanged, outputType);
            }
            i = end + 1;
        }
        System.err.println("The experimental design is finished. The last experimentations do not exist in the file.");
        myPop.writResult.close();
        myPop = null;
    }

    int[] typeId;

    public ResistingHostility(float alpha, float epsilon, int nbPoint, int firstLine, int endLine, boolean knowTheOthersHaveChanged, int outputType) {
        this.alpha = alpha;
        this.epsilon = epsilon;
        this.nbPoints = nbPoint; // nbPoints to sample in j's tolerance segment to define the opinions of i about j 
        expeFini = initAgentsFromFile(firstLine, endLine, knowTheOthersHaveChanged);
        if (!expeFini) {
            typeId = new int[6];
            Arrays.fill(typeId, -1);
            typeId[0] = 0;
            String lastType = population[0].type;
            String group = population[0].group;
            int count = 1;
            for (int i = 1; i < sizePop; i++) {
                if (!population[i].type.equalsIgnoreCase(lastType) || !population[i].group.equalsIgnoreCase(group)) {
                    typeId[count] = i;
                    count++;
                    lastType = population[i].type;
                    group = population[i].group;
                }
            }
            for (int z = 0; z < typeId.length; z++) {
                for (int a = 0; a < typeId.length; a++) {
                    population[typeId[z]].previousOpinionOnOtherAgents[a] = population[typeId[z]].opinionOnOthers(population[typeId[a]], knowTheOthersHaveChanged);
                }
            }
            messageFile = myPop.messageFileName;
            nomExpe = myPop.country;
            initMessage();
            this.nbIter = nbMessages;
            computeAgentsOpinions();
            if (outputType == 0) { // the user wishes to have the average values of opinion over the population
                iteration(knowTheOthersHaveChanged);
            } else { // it is equal to 1 and the user asks for the detailed value for each type of agents
                iterationWithAgentDetail(knowTheOthersHaveChanged);
            }
        }
    }

    public boolean initAgentsFromFile(int firstLine, int endLine, boolean knowsAboutOthers) {
        myPop.go = true;
        boolean fini = countPop(firstLine, endLine);
        if (!fini) {
            population = new Agent[sizePop];
            String[] name = new String[nbWorldView];
            name[0] = new String("muslims");
            name[1] = new String("christians");
            name[2] = new String("areligious");
            int count = 0;
            int z = 0;
            while (myPop.go) {
                z = 0;
                float[][] popToGenere = myPop.lireExpDesignInitPop(firstLine, endLine, popFile, nbWorldView);
                sizePop = (int) popToGenere[0][12];
                population = new Agent[sizePop];
                for (int i = 0; i < popToGenere.length; i++) { // i represents each type of agents
                    int dimAdhes = (int) popToGenere[i][0];
                    int nb = (int) (popToGenere[i][1]); // 
                    int type = (int) popToGenere[i][2];
                    maPosition = new float[nbWorldView];
                    lowTol = new float[nbWorldView];
                    highTol = new float[nbWorldView];
                    Arrays.fill(maPosition, 0.0f);
                    Arrays.fill(lowTol, 0.0f);
                    Arrays.fill(highTol, 0.0f);
                    z = 0;
                    for (int v = 0; v < nbWorldView;) {
                        lowTol[v] = popToGenere[i][3 + v + z];
                        maPosition[v] = popToGenere[i][4 + v + z];
                        highTol[v] = popToGenere[i][5 + v + z];
                        v = v + 1;
                        z = z + 2;
                    }

                    int j = 0;
                    for (j = 0; j < nb; j++) {
                        initAgentRandomized();
                        population[count] = new Agent(this, type, count, name, maPosition, lowTol, highTol, sizePop, alpha, dimAdhes, nb);
                        count++;
                    }

                }

                firstLine = firstLine + popToGenere.length;
                if (firstLine > endLine) {
                    myPop.go = false;
                }
            }
        }
        return fini;
    }

    public boolean countPop(int fLine, int endL) {
        Tools myPop = new Tools();
        int firstLine = fLine;
        int count = 0;
        int countInclusif = 0;
        int countExclusif = 0;
        while (myPop.go) {
            float[][] popToGenere = myPop.lireExpDesignInitPop(firstLine, endL, popFile, nbWorldView);
            if (!myPop.endFile) {
                for (int i = 0; i < popToGenere.length; i++) {
                    //country = (int) popToGenere[i][0];
                    int nb = (int) popToGenere[i][1];
                    count = count + nb;
                    if ((int) popToGenere[i][2] == 1) { // inclusive
                        countInclusif = countInclusif + nb;
                    } else { // exclusive
                        countExclusif = countExclusif + nb;
                    }
                    if (i == popToGenere.length - 1) {
                        sizePop = count;
                        inclusifs = countInclusif;
                        exclusifs = countExclusif;
                        count = 0;
                    }
                }
            }
        }
        return myPop.endFile;
    }

    float[] maPosition;
    float[] lowTol;
    float[] highTol;

    public void initAgentRandomized() {
        // This is to randomize slightly the entering values of the cultural views profile 

        float rand = -randomized + ((float) myRand.nextDouble() * (2 * randomized));
        double prop = 0.0;
        for (int i = 0; i < nbWorldView; i++) {
            prop = (maPosition[i] - lowTol[i]) / (highTol[i] - lowTol[i]);
            lowTol[i] = lowTol[i] + rand;
            rand = -randomized + ((float) myRand.nextDouble() * (2 * randomized));
            highTol[i] = highTol[i] + rand;
            while (highTol[i] <= lowTol[i]) {
                rand = -randomized + ((float) myRand.nextDouble() * (2 * randomized));
                highTol[i] = highTol[i] + rand;
            }
            maPosition[i] = lowTol[i] + (float) (prop * (highTol[i] - lowTol[i]));
        }
        //
    }

    // compute the opinions of the agents on the others as an indicator
    public void computeAgentsOpinions() {
        for (int i = 0; i < sizePop; i++) {
            for (int j = 0; j < sizePop; j++) {
                population[i].opinionGlobalOnOthers[j] = (float) population[i].opinionOnOthers(population[j]);
            }
        }
    }

// compute the opinions of the agents on the others as an indicator
    public double[] computeAgentsOpinions(int agentComputer, boolean knowTheOthersHasChanged) {
        double[] op = new double[sizePop];
        Arrays.fill(op, 1.0);
        for (int i = 0; i < sizePop; i++) {
            if (i != agentComputer) {
                op[i] = (float) population[agentComputer].opinionOnOthers(population[i], knowTheOthersHasChanged);
            }
        }
        return op;
    }

    // compute the opinions of one group for another group (or the same) one group by cultural dimension composed of people having their highest maPosition in the group dimension
    public float[] computeAgentsOpinionsAvgStd(int dimGroupComputer, int dimOtherGroup, boolean knowTheOthersHasChanged) {
        float op[] = new float[2];
        Arrays.fill(op, 0.0f);
        int divisor = 0;
        for (int i = 0; i < sizePop; i++) {
            if (population[i].mainCWWFromGroup() == dimGroupComputer) {
                for (int j = 0; j < sizePop; j++) {
                    if (population[j].mainCWWFromGroup() == dimOtherGroup) {
                        op[0] = op[0] + (float) population[i].opinionOnOthers(population[j], knowTheOthersHasChanged);
                        divisor++;
                    }
                }
            }
        }
        op[0] = op[0] / (float) divisor; // computation of the average
        for (int i = 0; i < sizePop; i++) {
            if (population[i].mainCWWFromGroup() == dimGroupComputer) {
                for (int j = 0; j < sizePop; j++) {
                    if (population[j].mainCWWFromGroup() == dimOtherGroup) {
                        op[1] = op[1] + (float) Math.pow(population[i].opinionOnOthers(population[j], knowTheOthersHasChanged) - op[0], 2.0);
                    }
                }
            }
        }
        op[1] = (float) Math.sqrt(op[1] / (float) divisor); // computation of the average
        return op;
    }

    // compute the opinions of one group for another group (or the same) one group by cultural dimension composed of people having their highest maPosition in the group dimension
    public float[] computeAgentsOpinionsAvgStdSimplified(int dimGroupComputer, int dimOtherGroup, boolean knowTheOthersHasChanged) {
        float op[] = new float[2];
        Arrays.fill(op, 0.0f);
        int divisor = 0;
        int nbCouples = 0;
        for (int i = 0; i < typeId.length; i++) {
            if (population[typeId[i]].mainCWWFromGroup() == dimGroupComputer) {
                for (int j = 0; j < typeId.length; j++) {
                    //for (int j = 0; j < sizePop; j++) {
                    if (population[typeId[j]].mainCWWFromGroup() == dimOtherGroup) {
                        nbCouples = (population[typeId[i]].nbAgentThisType * population[typeId[j]].nbAgentThisType);
                        op[0] = op[0] + nbCouples * (float) population[typeId[i]].opinionOnOthers(population[typeId[j]], knowTheOthersHasChanged);
                        divisor = divisor + nbCouples;
                    }
                }
            }
        }
        op[0] = op[0] / (float) divisor; // computation of the average
        nbCouples = 0;
        for (int i = 0; i < typeId.length; i++) {
            if (population[typeId[i]].mainCWWFromGroup() == dimGroupComputer) {
                for (int j = 0; j < typeId.length; j++) {
                    if (population[typeId[j]].mainCWWFromGroup() == dimOtherGroup) {
                        nbCouples = (population[typeId[i]].nbAgentThisType * population[typeId[j]].nbAgentThisType);
                        op[1] = op[1] + nbCouples * (float) Math.pow(population[typeId[i]].opinionOnOthers(population[typeId[j]], knowTheOthersHasChanged) - op[0], 2.0);
                    }
                }
            }
        }
        op[1] = (float) Math.sqrt(op[1] / (float) divisor); // computation of the average
        return op;
    }

    // compute the opinions of one group for all the population
    public float[] computeAgentsOpinionsAvgStdOfAll(int dimGroupComputer, boolean knowTheOthersHasChanged) {
        float op[] = new float[2];
        Arrays.fill(op, 0.0f);
        int divisor = 0;
        int nbCouples = 0;
        for (int i = 0; i < typeId.length; i++) {
            if (population[typeId[i]].mainCWWFromGroup() == dimGroupComputer) {
                for (int j = 0; j < typeId.length; j++) {
                    nbCouples = (population[typeId[i]].nbAgentThisType * population[typeId[j]].nbAgentThisType);
                    op[0] = op[0] + nbCouples * (float) population[typeId[i]].opinionOnOthers(population[typeId[j]], knowTheOthersHasChanged);
                    divisor = divisor + nbCouples;
                }
            }
        }
        op[0] = op[0] / (float) divisor; // computation of the average
        nbCouples = 0;
        for (int i = 0; i < typeId.length; i++) {
            if (population[typeId[i]].mainCWWFromGroup() == dimGroupComputer) {
                for (int j = 0; j < typeId.length; j++) {
                    nbCouples = (population[typeId[i]].nbAgentThisType * population[typeId[j]].nbAgentThisType);
                    op[1] = op[1] + nbCouples * (float) Math.pow(population[typeId[i]].opinionOnOthers(population[typeId[j]], knowTheOthersHasChanged) - op[0], 2.0);
                }
            }
        }
        op[1] = (float) Math.sqrt(op[1] / (float) divisor); // computation of the average
        return op;
    }

    // compute the opinions of everyone one on one particular group
    public float[] computeAgentsOpinionsAvgStdOnOneGroup(int dimGroupToEvaluate, boolean knowTheOthersHasChanged) {
        float op[] = new float[2];
        Arrays.fill(op, 0.0f);
        int divisor = 0;
        int nbCouples = 0;
        for (int i = 0; i < typeId.length; i++) {
            for (int j = 0; j < typeId.length; j++) {
                if (population[typeId[j]].mainCWWFromGroup() == dimGroupToEvaluate) {
                    nbCouples = (population[typeId[i]].nbAgentThisType * population[typeId[j]].nbAgentThisType);
                    op[0] = op[0] + nbCouples * (float) population[typeId[i]].opinionOnOthers(population[typeId[j]], knowTheOthersHasChanged);
                    divisor = divisor + nbCouples;
                }
            }
        }
        op[0] = op[0] / (float) divisor; // computation of the average
        nbCouples = 0;
        for (int i = 0; i < typeId.length; i++) {
            for (int j = 0; j < typeId.length; j++) {
                if (population[typeId[j]].mainCWWFromGroup() == dimGroupToEvaluate) {
                    nbCouples = (population[typeId[i]].nbAgentThisType * population[typeId[j]].nbAgentThisType);
                    op[1] = op[1] + nbCouples * (float) Math.pow(population[typeId[i]].opinionOnOthers(population[typeId[j]], knowTheOthersHasChanged) - op[0], 2.0);
                }
            }
        }
        op[1] = (float) Math.sqrt(op[1] / (float) divisor); // computation of the average
        return op;
    }

    // compute the opinions the agents has on one agent : the source
    public float computeAgentsOpinionsOnASource(Agent source) {
        float opSource = 0.0f;
        for (int i = 0; i < sizePop; i++) {
            opSource = opSource + (float) population[i].opinionOnOthers(source);
        }
        return opSource / (float) (sizePop - 1);
    }

    public double computeAverageAgentsOpinions() { // except oneself
        computeAgentsOpinions();
        double count = 0;
        for (int i = 0; i < sizePop; i++) {
            for (int j = 0; j < sizePop; j++) {
                if (i != j) {
                    count = count + population[i].opinionGlobalOnOthers[j];
                }
            }
        }
        count = count / (double) (sizePop * (sizePop - 1));
        return count;
    }

    public void initMessage() {
        messages = new ArrayList<Message>();
        float[][] popToGenere = myPop.lireExpDesignInitMessages(messageFile);
        nbMessages = popToGenere.length;
        int deb = 0;
        String typeAgent = new String();
        float maxAdher = Float.MIN_VALUE;
        int dimAdhes = -1;
        for (int i = 0; i < nbMessages; i++) {
            maxAdher = Float.MIN_VALUE;
            deb = 1;
            int nbCWW = (int) ((popToGenere[i].length - 1) / 3.0f);
            float[] adherence = new float[nbCWW];
            float[] alow = new float[nbCWW];
            float[] amax = new float[nbCWW];
            for (int j = 0; j < nbCWW; j++) {
                adherence[j] = popToGenere[i][deb + 1];
                if (adherence[j] > maxAdher) {
                    maxAdher = adherence[j];
                    dimAdhes = i;
                }
                alow[j] = popToGenere[i][deb];
                amax[j] = popToGenere[i][deb + 2];
                deb = deb + 3;
            }

            if (popToGenere[i][0] == 1) {
                typeAgent = "agressor";
            } else {
                typeAgent = "victim";
            }
            Agent source = new Agent(this, typeAgent, adherence, alow, amax, alpha, dimAdhes, 1);
            messages.add(new Message(i + 1, (int) (popToGenere[i][0]), source));
        }
    }

    public double computeDistanceAutrui(int i, boolean knowTheOthersHaveChanged) {
        double[] opinions = computeAgentsOpinions(i, knowTheOthersHaveChanged);
        double distanceAutrui = 0.0;
        for (int z = 0; z < opinions.length; z++) {
            if (z != i) {
                distanceAutrui = distanceAutrui + Math.abs(1 - opinions[z]);
            }
        }
        distanceAutrui = distanceAutrui / (double) (opinions.length - 1);
        return distanceAutrui;
    }

    public double computeIntraGroupDistance(int i, boolean knowTheOthersHaveChanged) {
        double distanceIntraGroup = 0.0;
        int nbIndiv = 0;
        for (int z = 0; z < sizePop; z++) {
            if (population[z].mainCWWFromGroup() == population[i].mainCWWFromGroup()) {
                distanceIntraGroup = distanceIntraGroup + (Math.abs(1.0 - population[i].opinionOnOthers(population[z], knowTheOthersHaveChanged)));
                nbIndiv++;
            }
        }
        distanceIntraGroup = distanceIntraGroup / (double) nbIndiv;
        return distanceIntraGroup;
    }

    public double computeOpPop(int i, boolean knowTheOthersHaveChanged) { // compute the average opinion of one agent type over all the population
        double[] opinions = computeAgentsOpinions(i, knowTheOthersHaveChanged);
        double cumul = 0.0;
        for (int z = 0; z < opinions.length; z++) {
            cumul = cumul + opinions[z];
        }
        cumul = cumul / (double) (opinions.length);
        return cumul;
    }

    public double[] computeOpGroup(int i, boolean knowTheOthersHaveChanged) { // compute the average opinions of one agent type over each groups of agent
        double[] opinions = computeAgentsOpinions(i, knowTheOthersHaveChanged);
        double[] cumul = new double[nbWorldView];
        int[] nbByGroup = new int[nbWorldView];
        Arrays.fill(nbByGroup, 0);
        Arrays.fill(cumul, 0.0);
        int temp = -1;
        for (int z = 0; z < opinions.length; z++) {
            temp = population[z].mainCWWFromGroup();
            cumul[temp] = cumul[temp] + opinions[z];
            nbByGroup[temp] = nbByGroup[temp] + 1;
        }
        for (int j = 0; j < nbWorldView; j++) {
            cumul[j] = cumul[j] / (double) (nbByGroup[j]);
        }
        return cumul;
    }

    public void iterationWithAgentDetail(boolean knowTheOthersHaveChanged) {

        // impression etat init
        iteration = 0;
        double opSourceBeforeMessage = -2.0f;
        double opSourceAfterMessage = -2.0f;
        double opSourceThreatBeforeMessage = -2.0f;
        double opSourceThreatAfterMessage = -2.0f;
        double opSourceMuslimInclusifBeforeMessage = -2.0f;
        double opSourceMuslimInclusifAfterMessage = -2.0f;
        double opSourceMuslimExclusifBeforeMessage = -2.0f;
        double opSourceMuslimExclusifAfterMessage = -2.0f;
        double opSourceAreligiousInclusifBeforeMessage = -2.0f;
        double opSourceAreligiousInclusifAfterMessage = -2.0f;
        double opSourceAreligiousExclusifBeforeMessage = -2.0f;
        double opSourceAreligiousExclusifAfterMessage = -2.0f;
        double opSourceChristiansInclusifBeforeMessage = -2.0f;
        double opSourceChristiansInclusifAfterMessage = -2.0f;
        double opSourceChristiansExclusifBeforeMessage = -2.0f;
        double opSourceChristiansExclusifAfterMessage = -2.0f;

        double distInGroupDontSeeOthers = -1000;
        double distAutrui = -1000.0;
        double opPop = 0.0;
        double[] opGroup = new double[nbWorldView];
        double stdPop;
        double[] stdGroup = new double[nbWorldView];
        Arrays.fill(opGroup, 0.0);
        Arrays.fill(stdGroup, 0.0);

        //recolter un individu de chaque type et les mettre dans loop ci-dessous
        int[] typeId = new int[6];
        Arrays.fill(typeId, -1);
        typeId[0] = 0;
        String lastType = population[0].type;
        String group = population[0].group;
        int count = 1;
        for (int i = 1; i < sizePop; i++) {
            if (!population[i].type.equalsIgnoreCase(lastType) || !population[i].group.equalsIgnoreCase(group)) {
                //System.err.println(lastType + " " + population[i].type + " " + group + " " + population[i].group);
                typeId[count] = i;
                count++;
                lastType = population[i].type;
                group = population[i].group;
            }
        }
        for (int z = 0; z < typeId.length; z++) { // everyone is impacted by the message except the "emetteur" who is not is the population
            int i = typeId[z];
            opSourceThreatBeforeMessage = population[i].opinionOnOthers(messages.get(0).emetteur);
            opSourceMuslimInclusifBeforeMessage = population[i].opinionOnOthers(population[4]);
            opSourceMuslimExclusifBeforeMessage = population[i].opinionOnOthers(population[5]);
            opSourceChristiansInclusifBeforeMessage = population[i].opinionOnOthers(population[2]);
            opSourceChristiansExclusifBeforeMessage = population[i].opinionOnOthers(population[3]);
            opSourceAreligiousInclusifBeforeMessage = population[i].opinionOnOthers(population[0]);
            opSourceAreligiousExclusifBeforeMessage = population[i].opinionOnOthers(population[1]);

            distAutrui = computeDistanceAutrui(i, knowTheOthersHaveChanged);
            distInGroupDontSeeOthers = computeIntraGroupDistance(i, knowTheOthersHaveChanged);
            population[i].distAutruiInit = distAutrui;
            opPop = computeOpPop(i, knowTheOthersHaveChanged);
            opGroup = computeOpGroup(i, knowTheOthersHaveChanged);

            opSourceThreatAfterMessage = opSourceThreatBeforeMessage;
            opSourceMuslimInclusifAfterMessage = opSourceMuslimInclusifBeforeMessage;
            opSourceMuslimExclusifAfterMessage = opSourceMuslimExclusifBeforeMessage;
            opSourceChristiansInclusifAfterMessage = opSourceChristiansInclusifBeforeMessage;
            opSourceChristiansExclusifAfterMessage = opSourceChristiansExclusifBeforeMessage;
            opSourceAreligiousInclusifAfterMessage = opSourceAreligiousInclusifBeforeMessage;
            opSourceAreligiousExclusifAfterMessage = opSourceAreligiousExclusifBeforeMessage;

            writeImpactMessageWithAgentDetails(population[i], messages.get(0), (float) opSourceBeforeMessage, (float) opSourceAfterMessage,
                    (float) opSourceThreatBeforeMessage, (float) opSourceThreatAfterMessage,
                    (float) opSourceMuslimInclusifBeforeMessage, (float) opSourceMuslimInclusifAfterMessage,
                    (float) opSourceMuslimExclusifBeforeMessage, (float) opSourceMuslimExclusifAfterMessage,
                    (float) opSourceAreligiousInclusifBeforeMessage, (float) opSourceAreligiousInclusifAfterMessage,
                    (float) opSourceAreligiousExclusifBeforeMessage, (float) opSourceAreligiousExclusifAfterMessage,
                    (float) opSourceChristiansInclusifBeforeMessage, (float) opSourceChristiansInclusifAfterMessage,
                    (float) opSourceChristiansExclusifBeforeMessage, (float) opSourceChristiansExclusifAfterMessage,
                    (float) distAutrui, (float) distAutrui,
                    (float) distInGroupDontSeeOthers, (float) distInGroupDontSeeOthers,
                    (float) opPop, opGroup, knowTheOthersHaveChanged);
        }
        int mes = 0;
        for (int it = 1; it <= nbIter; it++) {
            iteration = it;
            mes = mediaMessageWithAgentDetails(mes, it, knowTheOthersHaveChanged);
        }
    }

    public void iteration(boolean knowTheOthersHaveChanged) {
        // impression etat init
        iteration = 0;
        writeImpactMessageAVGSTD(messages.get(0), knowTheOthersHaveChanged);
        // fin impression etat init     
        int mes = 0;
        for (int i = 1; i <= nbIter; i++) {
            iteration = i;
            mes = mediaMessage(mes, i, knowTheOthersHaveChanged); // send to agent I a value on agent J
        }
    }

    public int mediaMessageWithAgentDetails(int mes, int iter, boolean knowTheOthersHaveChanged) {
        int nextMes = mes;
        if (mes < messages.size()) {
            double opSourceBeforeMessage = -2.0f;
            double opSourceAfterMessage = -2.0f;
            double opSourceThreatBeforeMessage = -2.0f;
            double opSourceThreatAfterMessage = -2.0f;
            double opSourceMuslimInclusifBeforeMessage = -2.0f;
            double opSourceMuslimInclusifAfterMessage = -2.0f;
            double opSourceMuslimExclusifBeforeMessage = -2.0f;
            double opSourceMuslimExclusifAfterMessage = -2.0f;
            double opSourceAreligiousInclusifBeforeMessage = -2.0f;
            double opSourceAreligiousInclusifAfterMessage = -2.0f;
            double opSourceAreligiousExclusifBeforeMessage = -2.0f;
            double opSourceAreligiousExclusifAfterMessage = -2.0f;
            double opSourceChristiansInclusifBeforeMessage = -2.0f;
            double opSourceChristiansInclusifAfterMessage = -2.0f;
            double opSourceChristiansExclusifBeforeMessage = -2.0f;
            double opSourceChristiansExclusifAfterMessage = -2.0f;

            double distAutruiIfAllOthersAreMet = -1000.0; // see my change but also the change of others
            double distAutruiJustEvolvingDueTheMessageImpact = -1000.0;; // only see my change, do not see the change of others
            double distInGroupSeeOthers = -1000;
            double distInGroupDontSeeOthers = -1000;
            double opPop = 0.0;
            double[] opGroup = new double[nbWorldView];
            double stdPop;
            double[] stdGroup = new double[nbWorldView];
            Arrays.fill(opGroup, 0.0);
            Arrays.fill(stdGroup, 0.0);

            if (messages.get(mes).time == iter) {
                for (int z = 0; z < typeId.length; z++) { // everyone is impacted by the message except the "emetteur" who is not is the population
                    int i = typeId[z]; // the type of agent

                    opSourceBeforeMessage = population[i].opinionOnOthers(messages.get(mes).emetteur);
                    opSourceThreatBeforeMessage = population[i].opinionOnOthers(messages.get(0).emetteur);
                    opSourceMuslimInclusifBeforeMessage = population[i].opinionOnOthers(population[4]);
                    opSourceMuslimExclusifBeforeMessage = population[i].opinionOnOthers(population[5]);
                    opSourceAreligiousInclusifBeforeMessage = population[i].opinionOnOthers(population[0]);
                    opSourceAreligiousExclusifBeforeMessage = population[i].opinionOnOthers(population[1]);
                    opSourceChristiansInclusifBeforeMessage = population[i].opinionOnOthers(population[2]);
                    opSourceChristiansExclusifBeforeMessage = population[i].opinionOnOthers(population[3]);

                    population[i].reactionToMessage(messages.get(mes).type, messages.get(mes).emetteur);
                    distAutruiIfAllOthersAreMet = computeDistanceAutrui(i, !knowTheOthersHaveChanged);
                    distAutruiJustEvolvingDueTheMessageImpact = computeDistanceAutrui(i, knowTheOthersHaveChanged);
                    distInGroupSeeOthers = computeIntraGroupDistance(i, !knowTheOthersHaveChanged);
                    distInGroupDontSeeOthers = computeIntraGroupDistance(i, knowTheOthersHaveChanged);
                    opPop = computeOpPop(i, knowTheOthersHaveChanged);
                    opGroup = computeOpGroup(i, knowTheOthersHaveChanged);

                    opSourceAfterMessage = population[i].opinionOnOthers(messages.get(mes).emetteur);
                    opSourceThreatAfterMessage = population[i].opinionOnOthers(messages.get(0).emetteur);
                    opSourceMuslimInclusifAfterMessage = population[i].opinionOnOthers(population[4]);
                    opSourceMuslimExclusifAfterMessage = population[i].opinionOnOthers(population[5]);
                    opSourceAreligiousInclusifAfterMessage = population[i].opinionOnOthers(population[0]);
                    opSourceAreligiousExclusifAfterMessage = population[i].opinionOnOthers(population[1]);
                    opSourceChristiansInclusifAfterMessage = population[i].opinionOnOthers(population[2]);
                    opSourceChristiansExclusifAfterMessage = population[i].opinionOnOthers(population[3]);
                    writeImpactMessageWithAgentDetails(population[i], messages.get(mes), (float) opSourceBeforeMessage, (float) opSourceAfterMessage,
                            (float) opSourceThreatBeforeMessage, (float) opSourceThreatAfterMessage,
                            (float) opSourceMuslimInclusifBeforeMessage, (float) opSourceMuslimInclusifAfterMessage,
                            (float) opSourceMuslimExclusifBeforeMessage, (float) opSourceMuslimExclusifAfterMessage,
                            (float) opSourceAreligiousInclusifBeforeMessage, (float) opSourceAreligiousInclusifAfterMessage,
                            (float) opSourceAreligiousExclusifBeforeMessage, (float) opSourceAreligiousExclusifAfterMessage,
                            (float) opSourceChristiansInclusifBeforeMessage, (float) opSourceChristiansInclusifAfterMessage,
                            (float) opSourceChristiansExclusifBeforeMessage, (float) opSourceChristiansExclusifAfterMessage,
                            (float) distAutruiIfAllOthersAreMet, (float) distAutruiJustEvolvingDueTheMessageImpact,
                            (float) distInGroupSeeOthers, (float) distInGroupDontSeeOthers,
                            (float) opPop, opGroup, knowTheOthersHaveChanged);
                }
                nextMes = mes + 1;
            }
        }
        return nextMes;
    }

    public int mediaMessage(int mes, int iter, boolean knowTheOthersHaveChanged) {
        int nextMes = mes;
        if (mes < messages.size()) {
            if (messages.get(mes).time == iter) {
                for (int i = 0; i < sizePop; i++) { // everyone is impacted by the message except the "emetteur" who is not is the population
                    population[i].reactionToMessage(messages.get(mes).type, messages.get(mes).emetteur);
                }
                if (iter == 7) {
                    writeImpactMessageAVGSTD(messages.get(0), knowTheOthersHaveChanged);
                }
                nextMes = mes + 1;
            }
        }
        return nextMes;
    }

    public void writeImpactMessageWithAgentDetails(Agent receiver, Message mes, float opSourceBeforeMessage, float opSourceAfterMessage,
            float opSourceThreatBeforeMessage, float opSourceThreatAfterMessage,
            float opSourceMuslimInclusifBeforeMessage, float opSourceMuslimInclusifAfterMessage,
            float opSourceMuslimExclusifBeforeMessage, float opSourceMuslimExclusifAfterMessage,
            float opSourceAreligiousInclusifBeforeMessage, float opSourceAreligiousInclusifAfterMessage,
            float opSourceAreligiousExclusifBeforeMessage, float opSourceAreligiousExclusifAfterMessage,
            float opSourceChristiansInclusifBeforeMessage, float opSourceChristiansInclusifAfterMessage,
            float opSourceChristiansExclusifBeforeMessage, float opSourceChristiansExclusifAfterMessage,
            float distAutruiOtherAndMeChanged, float distAutruiOnlyMeChanged,
            float distIntraGroupSeeOthers, float distIntraGroupSeeMeOnly,
            float opPop, double[] opGroup, boolean knowsAboutOthers) {
        //System.err.println("je passe dans ecrire avec detail ");
        myPop.writResult.print(popFile + ";");
        myPop.writResult.print(messageFile + ";");
        myPop.writResult.print(nomExpe + ";");
        printParametres();
        myPop.writResult.print(iteration + ";");
        myPop.writResult.print(receiver.nbAgentThisType + ";");
        myPop.writResult.print(receiver.type + ";");
        myPop.writResult.print(receiver.mainCWWFromGroup() + ";");
        for (int i = 0; i < receiver.myCulture.length; i++) {
            myPop.writResult.print(receiver.myCulture[i].alow + ";");
            myPop.writResult.print(receiver.myCulture[i].maPosition + ";");
            myPop.writResult.print(receiver.myCulture[i].amax + ";");
        }
        myPop.writResult.print(mes.type + ";");
        myPop.writResult.print(mes.emetteur.type + ";");
        myPop.writResult.print(mes.emetteur.mainCWWFromGroup() + ";");
        for (int i = 0; i < mes.emetteur.myCulture.length; i++) {
            myPop.writResult.print(mes.emetteur.myCulture[i].alow + ";");
            myPop.writResult.print(mes.emetteur.myCulture[i].maPosition + ";");
            myPop.writResult.print(mes.emetteur.myCulture[i].amax + ";");
        }
        myPop.writResult.print(opSourceBeforeMessage + ";");
        myPop.writResult.print(opSourceAfterMessage + ";");
        myPop.writResult.print(opSourceThreatBeforeMessage + ";");
        myPop.writResult.print(opSourceThreatAfterMessage + ";");
        myPop.writResult.print(opSourceMuslimInclusifBeforeMessage + ";");
        myPop.writResult.print(opSourceMuslimInclusifAfterMessage + ";");
        myPop.writResult.print(opSourceMuslimExclusifBeforeMessage + ";");
        myPop.writResult.print(opSourceMuslimExclusifAfterMessage + ";");
        myPop.writResult.print(opSourceAreligiousInclusifBeforeMessage + ";");
        myPop.writResult.print(opSourceAreligiousInclusifAfterMessage + ";");
        myPop.writResult.print(opSourceAreligiousExclusifBeforeMessage + ";");
        myPop.writResult.print(opSourceAreligiousExclusifAfterMessage + ";");
        myPop.writResult.print(opSourceChristiansInclusifBeforeMessage + ";");
        myPop.writResult.print(opSourceChristiansInclusifAfterMessage + ";");
        myPop.writResult.print(opSourceChristiansExclusifBeforeMessage + ";");
        myPop.writResult.print(opSourceChristiansExclusifAfterMessage + ";");
        myPop.writResult.print(distAutruiOnlyMeChanged + ";");
        myPop.writResult.print(distAutruiOtherAndMeChanged + ";");
        myPop.writResult.print(distIntraGroupSeeMeOnly + ";");
        myPop.writResult.print(distIntraGroupSeeOthers + ";");
        myPop.writResult.print(knowsAboutOthers + ";");
        myPop.writResult.print(opPop + ";");
        for (int i = 0; i < nbWorldView; i++) {
            myPop.writResult.print((float) opGroup[i] + ";");
        }
        myPop.writResult.println();
    }

    public void writeImpactMessageAVGSTD(Message mes, boolean knowsAboutOthers) { // AVGSTD
        myPop.writResult.print(popFile + ";");
        myPop.writResult.print(messageFile + ";");
        myPop.writResult.print(nomExpe + ";");
        printParametres();
        myPop.writResult.print(iteration + ";");
        for (int z = 0; z < typeId.length; z++) {
            myPop.writResult.print(population[typeId[z]].group + ";");
            myPop.writResult.print(population[typeId[z]].type + ";");
            myPop.writResult.print(population[typeId[z]].nbAgentThisType + ";");
        }
        float[] avgStd = new float[2];
        Arrays.fill(avgStd, 0.0f);
        // opinion of each group on another group
        for (int a = 0; a < nbWorldView; a++) {
            for (int b = 0; b < nbWorldView; b++) {
                avgStd = computeAgentsOpinionsAvgStdSimplified(a, b, knowsAboutOthers);
                myPop.writResult.print(new Float(avgStd[0]).toString().replace(".", ",") + ";");
                myPop.writResult.print(new Float(avgStd[1]).toString().replace(".", ",") + ";");
            }
        }
        // opinion of every one on each group
        for (int a = 0; a < nbWorldView; a++) {
            avgStd = computeAgentsOpinionsAvgStdOfAll(a, knowsAboutOthers);
            myPop.writResult.print(new Float(avgStd[0]).toString().replace(".", ",") + ";");
            myPop.writResult.print(new Float(avgStd[1]).toString().replace(".", ",") + ";");
        }
        // of the two other groups
        for (int a = 0; a < nbWorldView; a++) {
            avgStd = computeAgentsOpinionsAvgStdOnOneGroup(a, knowsAboutOthers);
            myPop.writResult.print(new Float(avgStd[0]).toString().replace(".", ",") + ";");
            myPop.writResult.print(new Float(avgStd[1]).toString().replace(".", ",") + ";");
        }
        myPop.writResult.println();
    }

    public void printParametres() {
        for (int i = 0; i < parametres.length; i++) {
            myPop.writResult.print(((new Float(parametres[i])).toString()).replace(".", ",") + ";");
        }
    }

    public double computeTolerance(int culturalDim) { // compute the average tolerance width of everyone for a cultural dimension
        double temp = 0;
        double tol = 0;
        for (int i = 0; i < sizePop; i++) {
            temp = population[i].myCulture[culturalDim].amax - 0; // ne compte que la partie positive de la tolerance
            if (temp > 0) {
                tol = tol + temp;
            }
        }
        tol = tol / (double) sizePop;
        return tol;
    }

}
