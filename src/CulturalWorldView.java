/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Sylvie.huet
 */
public class CulturalWorldView {

    //We consider a population of N agents, each agent i has an identity first defined by 3 values 
    // between -1 and +1, corresponding to its maPosition or rejection of a cultural world view. 
    // Agent's cultural worldviews: A positive value expresses maPosition while a negative one expresses rejection. 
    // The absolute value expresses the intensity of the maPosition of rejection. 
    // Tolerance: the agent is also defined by tolerance limits above and below this maPosition or rejection values. 
    float maPosition;
    float alow;
    float amax;
    float exAmax ;
    String name;

    public CulturalWorldView(String name, float adherence, float alow, float amax) {
        this.name = name;
        this.maPosition = adherence;
        this.alow = alow;
        this.amax = amax;
        if (Math.abs(adherence - amax) < 0.0000001f) { this.amax = adherence+0.00001f ; } // pour résoudre problème d'encodage de m.... 
        if (Math.abs(adherence - alow) < 0.0000001f) { this.alow = adherence-0.00001f ; }
    }

    public void changeBoundIfThreat(float mu, float adherenceSource, float epsilon) {
        exAmax = amax ;
        float base = 0.0f;
        float dist1 = Math.abs(adherenceSource - alow);
        float dist2 = Math.abs(adherenceSource - amax);
        float newBound = 0.0f;
        if (dist1 < dist2) {
            base = alow;
        } else {
            base = amax;
        }
        if (dist1 == dist2) {
            if (Math.random() < 0.5) {
                base = alow;
            } else {
                base = amax;
            }
        }
        newBound = base + mu * (base - maPosition - epsilon * (Math.abs(base - maPosition) / (base - maPosition)));
        if (base == alow) {
            alow = newBound;
        } else {
            amax = newBound;
        }
    }

}
